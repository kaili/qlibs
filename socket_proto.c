/*
 *  Revision 20180319, Kai Peter
 *  - switched to new ndelay() function
 *  Revision 20180305, Kai Peter
 *  - changed function types to 'size_t', code clean-up
 *  Revision 20170922, Kai Peter
 *  - removed '#ifdef' for IPv6 (thanks Erwin Hoffmann)
 *  Revision 20160728, Kai Peter
 *  - new file: consolidated 'socket_tcp*.c', 'socket_udp*.c'
*/
#include "socket.h"
#include "ndelay.h"
#include "errmsg.h"
#include "close.h"         /* better use unistd.h ? */

#ifndef EAFNOSUPPORT
#define EAFNOSUPPORT EINVAL
#endif

/* ***** tcp **************************************************************** */
int socket_tcp4(void) {
  size_t s;

  s = socket(PF_INET,SOCK_STREAM,0);
  if (s == -1) return -1;
  if (ndelay(s,ON) == -1) { close(s); return -1; }
  return s;
}

int socket_tcp6(void) {
  size_t s;

  if (ipv4socket) return socket_tcp4();
  s = socket(PF_INET6,SOCK_STREAM,0);
  if (s == -1) {
    if (errno == EINVAL || errno == EAFNOSUPPORT || errno == EPFNOSUPPORT || errno == EPROTONOSUPPORT )
      return socket_tcp4();
  }
  if (ndelay(s,ON) == -1) { close(s); return -1; }
  return s;
}

/* ***** udp **************************************************************** */
int socket_udp4(void) {
  size_t s;

  s = socket(PF_INET,SOCK_DGRAM,0);
  if (s == -1) return -1;
  if (ndelay(s,ON) == -1) { close(s); return -1; }
  return s;
}

int socket_udp6(void) {
  size_t s;

  if (ipv4socket) return socket_udp4();
  s = socket(PF_INET6,SOCK_DGRAM,0);
  if (s == -1)
    if (errno == EINVAL || errno == EAFNOSUPPORT || errno == EPFNOSUPPORT || errno == EPROTONOSUPPORT)
      return socket_udp4();

  if (ndelay(s,ON) == -1) { close(s); return -1; }
  return s;
}
