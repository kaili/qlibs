# Makefile for qlibs

GNUMAKEFLAGS += --no-print-directory

SHELL ?= sh

SRCS := alloc.c byte.c case.c env.c errmsg.c errstr.c fd.c fmt.c
SRCS += lock.c ndelay.c open.c qstrings.c scan.c seek.c sig.c
SRCS += str.c stralloc.c strlen.c uint16p.c uint32p.c wait.c

MODULES = buffer cdb base64 getln pathexec prot readclose dns getopt ip socket time
#MODULES = buffer cdb dns ip socket time
ifeq ("$(shell [ -f $(OLDPWD)/conf-qlibs ] && echo "HASCONF")","HASCONF")
MODULES = $(shell cat $(OLDPWD)/conf-qlibs | sed s}\^\#\.*$$}""}g | sed s}\\\n}" "}g)
endif

OBJS := $(SRCS:.c=.o) mimalloc.o

COMPILE=./compile
MAKELIB=./makelib
.PHONY: install mimalloc

default all: check qlibs
	@echo `date "+%Y%m%d %H:%M"` > .build

check:
	@[ -f $(COMPILE) ] && [ -f $(MAKELIB) ] || ./configure

clean:
	@echo -n Cleaning up qlibs ...
	@rm -f *.o *.a compile makelib *.tmp
	@cd dns ; $(MAKE) clean
	@[ -d compat ] && cd compat ; $(MAKE) clean
	@[ -d tests ] && cd tests ; $(MAKE) clean
	@echo " done!"

compat: all
	@cd compat ; $(MAKE)

install:
	@[ "$$QLIBS_LOCAL_INSTALL" = "yes" ] && \
	echo "Installing qlibs to $(OLDPWD)" && \
	cp -v libqlibs.a $(OLDPWD) || \
	sh ./install

core: check
	@echo Making qlibs ...
	$(COMPILE) $(SRCS)

libs qlibs qlibs.a: core mimalloc $(MODULES)
	@echo Creating final lib ...
	$(MAKELIB) libqlibs.a *.o

buffer: core
	@echo Making module $@ ...
	$(COMPILE) buffer.c buffer_get.c buffer_put.c
	$(MAKELIB) buffer.a buffer.o buffer_get.o buffer_put.o

cdb: core
	@echo Making module $@ ...
	$(COMPILE) cdbhash.c cdbmake.c cdbread.c buffer.c
	$(MAKELIB) cdb.a cdbread.o cdbmake.o cdbhash.o uint32p.o buffer.o
	@rm -f cdbread.o cdbmake.o cdbhash.o buffer.o

base64 getln pathexec prot readclose: core
	@echo Making module $@ ...
	$(COMPILE) $@.c

dns dns.a: core
	@echo "Building dns lib ..."
	@cd dns ; $(MAKE)
	@cp dns/dns.a dns.a
	ln -s dns.a dns/libqdns.a

getopt getoptb: core
	@echo Making module $@ ...
	$(COMPILE) getoptb.c

ip ip.a: core
	@echo Making module $@ ...
	$(COMPILE) ip4.c ip6.c socket.c
	$(MAKELIB) libqip.a ip4.o ip6.o socket.o

socket socket.a: core ip.a time.a
	@echo "Building socket lib ..."
	$(COMPILE) socket.c socket_connect.c socket_proto.c qsock-io.c
	$(MAKELIB) libqsocket.a socket.o socket_connect.o socket_proto.o qsock-io.o \
	ndelay.o ip4.o ip6.o iopause.o tai.o taia.o

time time.a iopause: core
	@echo Making module $@ ...
	$(COMPILE) iopause.c tai.c taia.c
	$(MAKELIB) libqtime.a iopause.o tai.o taia.o

mimalloc:
	$(COMPILE) mimalloc/src/static.c -o $@.o
