/* errmsg.c - print error messages to stderr
 *
 *  Revision 20200711, Kai Peter
 *  - changed the check to suppress 'notice:' with macro 'log'
 *  - add 'return 0' if (the error) code is zero with informal logging
 *  Revision 20180322, Kai Peter
 *  - changed: never exit negative
 *  - added special handling for macro 'log' (see errmsg.h)
 *  Revision 20180301, Kai Peter
 *  - replaced function 'fmt_ulong' by 'fmt_slong'
 *  - simplified code (string formatting and output)
 *  Revision 20170502, Kai Peter
 *  - added handling for negative error codes
 *  Revision 20170422, Kai Peter
 *  - new file
*/
#include <unistd.h>
#include <stdarg.h>
#include "errmsg.h"
#include "buffer.h"
#include "fmt.h"
#include "qstrings.h"

#ifndef WHO
#define WHO "errmsg: out of memory: "
#endif

unsigned int loglevel;

char *build_error_msg(char *x[])
{
  stralloc sa = {0};

  stralloc_copys(&sa,"");  /* usually not required, but who knows ;) */

  while(*x) { if(!stralloc_cats(&sa,*x++)) errsys(errno); } /* concatenate *x */

  if(!stralloc_0(&sa)) errsys(errno);
  return(sa.s);
}

size_t errmsg(char *who, int code, unsigned int severity, char *msg) {
  char strnum[FMT_SLONG];
  char *codestr = "";
  char *errorstr = "";
  char *logstr;

  if (!loglevel) loglevel = DEF_LOGLEVEL;

  errno = 0;   /* re-initialize errno, value is in 'code' now */
  if (loglevel < severity) return 0;

  switch(severity) {
  case FATAL:  logstr = "fatal: ";    break;   /* hard error */
  case ALERT:  logstr = "alert: ";    break;
  case CRIT:   logstr = "critical: "; break;
  case ERROR:  logstr = "error: ";    break;   /* temp/soft error */
  case WARN:   logstr = "warning: ";  break;
  case NOTICE: logstr = "notice: ";   break;   /* standard logging */
  case INFO:   logstr = "info: ";     break;   /* be more verbose */
  case DEBUG:  logstr = "debug: ";    break;
  default: /* shouldn't happen */
      severity = WARN;
      logstr="unknown: ";
    break;
  }

  if ((!code) && (severity < ERROR)) logstr = "";

  /* standard logging: special for macro 'log' (only) - suppress 'notice:' */
  if ((code == 0) && (severity == NOTICE)) logstr = "";

  /* handle the (error) code, fill up and format some variables */
  if (code != 0) {
    errorstr = errstr(code);                /* worst case: "unknown error" */
    strnum[fmt_slong(strnum,code)] = 0;           /* format int for output */
	codestr = B(" (",strnum,")");              /* code formatted as string */
  }

  /* if msg and errorstr is empty, (last) try to get a system message */
  if ((severity > ERROR) && str_equal(errorstr,""))
    if (str_equal(msg,"")) msg = errstr(code);   /* worst case: "no error" */

  buffer_puts(buffer_2,who);
  if (!str_equal(who,""))
    buffer_puts(buffer_2,": ");         /* suppress colon with log_anon()  */
  buffer_puts(buffer_2,logstr);
  buffer_puts(buffer_2,errorstr);       /* errorstr is empty if code = 0   */
  buffer_puts(buffer_2,codestr);        /* codestr is empty if code = 0    */
  if (!str_equal(codestr,""))           /* suppress colon if codestr ...   */
    if (!str_equal(msg,""))             /* ... and if msg is empty         */
      buffer_puts(buffer_2,": ");
  buffer_puts(buffer_2,msg);            /* msg could be empty - no matter  */
  buffer_putsflush(buffer_2,"\n");
  if (severity > WARN) return 0;        /* return 0 on informal severity   */
  if (code == 0) return 0;              /* return 0 on informal logging    */
  if (code == ESOFT) code = 100;
  if (code == EHARD) code = 111;
  if (severity > ERROR) return code;    /* return code on severity >= WARN */
  if ((code <= 0) || (code > 255)) _exit(127);
  _exit(code);
}
