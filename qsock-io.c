/*
 *  Revision 20180314, Kai Peter
 *  - added functions 'socket_recv()' and 'socket_send()' (thanks Erwin Hoffmann)
 *  Revision 20180307, Kai Peter
 *  - new file: functions 'timeoutread()' and 'timeoutwrite()'
*/
#include <unistd.h>
#include "socket.h"
#include "errmsg.h"
#include "iopause.h"
#include "ip.h"

int timeoutread(int fd, char *buf, size_t len, unsigned int timeout) {
//ssize_t timeoutread(int fd, char *buf, size_t len, unsigned int timeout) {
  struct taia now;
  struct taia deadline;
  iopause_fd x;

  x.fd = fd;
  x.events = IOPAUSE_READ;

  taia_now(&now);
  taia_uint(&deadline,timeout);
  taia_add(&deadline,&now,&deadline);

  for (;;) {
    taia_now(&now);
    iopause(&x,1,&deadline,&now);
    if (x.revents) break;
    if (taia_less(&deadline,&now)) {
	  errno = ETIMEDOUT;
	  return -1;
	}
  }
  return read(fd,buf,sizeof(buf));
}

int timeoutwrite(int fd, char *buf, size_t len, unsigned int timeout) {
  struct taia now;
  struct taia deadline;
  iopause_fd x;

  taia_now(&now);
  taia_uint(&deadline,timeout);
  taia_add(&deadline,&now,&deadline);

  x.fd = fd;
  x.events = IOPAUSE_WRITE;
  for (;;) {
    taia_now(&now);
    iopause(&x,1,&deadline,&now);
    if (x.revents) break;
    if (taia_less(&deadline,&now)) {
	  errno = ETIMEDOUT;
	  return -1;
	}
  }
  return write(fd,buf,sizeof(buf));
}

/* Warning: the functions below have experimental status */
/* ***** udp: */
ssize_t socket_recv(int s, char *buf, size_t len,
                    const char ip[16], uint16 *port, uint32 *scope_id)
{
  struct sockaddr_in6 sa;
  unsigned int dummy = sizeof(sa);
  int r;

  byte_zero(&sa,dummy);
  r = recvfrom(s,buf,len,0,(struct sockaddr *)&sa,&dummy);
  if (r == -1) return -1;

//  if (sa.sin6_family == AF_INET) {
  if (ipv4socket) {		// not really tested yet !!!!
    struct sockaddr_in *sa4 = (struct sockaddr_in *)&sa;
    byte_copy(ip,12,V4mappedprefix);
    byte_copy(ip+12,4,(char *)&sa4->sin_addr);
    uint16_unpack_big((char *)&sa4->sin_port,port);
    if (scope_id) *scope_id = 0;
    return r;
  }

  byte_copy(ip,16,(char *)&sa.sin6_addr);
  uint16_unpack_big((char *)&sa.sin6_port,port);
  if (scope_id) *scope_id = sa.sin6_scope_id;

  return r;
}
/*
int socket_send4(int s,const char *buf,int len,const char ip[4],uint16 port)
{
  struct sockaddr_in sa;

  byte_zero(&sa,sizeof(sa));
  sa.sin_family = AF_INET;
  uint16_pack_big((char *)&sa.sin_port,port);
  byte_copy((char *)&sa.sin_addr,4,ip);

  return sendto(s,buf,len,0,(struct sockaddr *)&sa,sizeof(sa));
}
*/
ssize_t socket_send(int s, const char *buf, size_t len,
                    const char ip[16], uint16 port, uint32 scope_id)
{
  struct sockaddr_in6 sa;

  if (ipv4socket) {
//  return socket_send4(s,buf,len,ip+12,port);
	struct sockaddr_in sa;

	byte_zero(&sa,sizeof(sa));
	sa.sin_family = AF_INET;
	uint16_pack_big((char *)&sa.sin_port,port);
	byte_copy((char *)&sa.sin_addr,4,ip);

	return sendto(s,buf,len,0,(struct sockaddr *)&sa,sizeof(sa));
  }

  byte_zero(&sa,sizeof(sa));
  sa.sin6_family = AF_INET6;
  sa.sin6_scope_id = scope_id;
  uint16_pack_big((char *)&sa.sin6_port,port);
  byte_copy((char *)&sa.sin6_addr,16,ip);
  return sendto(s,buf,len,0,(struct sockaddr *)&sa,sizeof(sa));
}

/* consolidated IPv4 and IPv6 call */
/*
int socket_send(int s,const char *buf,unsigned int len,const char ip[16],uint16 port,uint32 scope_id)
{ 
  if ((ipv4socket = ip6_isv4mapped(ip)))
    return socket_send4(s,buf,len,ip+12,port);
 
  return socket_send6(s,buf,len,ip,port,scope_id);
} 
*/