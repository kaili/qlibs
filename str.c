/*
 *  Revision 20190712, Kai Peter
 *  - renamed str_start() --> str_pcmp()
 *  - replaced 'str_diff' and 'str_diffn' by wrapper functions (commented out)
 *  - improved function declarations
 *  - gcc-6 compatibility: prevent '-Wmisleading-indentation' warning
 *  --> all these changes are applied from earlier outsite development!
*/
#include "str.h"
#include "strlen.h"
/* Consolidate the "str_*.c" functions into one source
   file. Original files shipped with qmail-1.03.
   The "str" functions will be linked to "str.a" only!
   Included files:    Size (bytes)    Date
     - str_chr.c              350     19980615
     - str_cpy.c              330     19980615
     - str_diff.c             439     19980615
     - str_diffn.c            550     19980615
     - str_len.c              239     19980615
     - str_rchr.c             397     19980615
     - str_start.c            342     19980615      */

/* Alternative: */
unsigned int str_chr(const char *s, int c) { return byte_chr(s,str_len(s),c); }

/* file: str_chr.c */
/*
unsigned int str_chr(register char *s,int c)
{
  register char ch;
  register char *t;

  ch = c;
  t = s;
  for (;;) {
    if (!*t) { break; } if (*t == ch) { break; } ++t;
    if (!*t) { break; } if (*t == ch) { break; } ++t;
    if (!*t) { break; } if (*t == ch) { break; } ++t;
    if (!*t) { break; } if (*t == ch) { break; } ++t;
  }
  return t - s;
}
*/
/* file: str_cpy.c */
/*
unsigned int str_copy(register char *s,register char *t)
{
  register int len;

  len = 0;
  for (;;) {
    if (!(*s = *t)) { return len; } ++s; ++t; ++len;
    if (!(*s = *t)) { return len; } ++s; ++t; ++len;
    if (!(*s = *t)) { return len; } ++s; ++t; ++len;
    if (!(*s = *t)) { return len; } ++s; ++t; ++len;
  }
}
*/
// possible alternative:
unsigned int str_copy(char *s,char *t) {
  byte_copy(s,str_len(t),t);
  return str_len(s);
}

/* file: str_diff.c */
/*
int str_diff(register char *s,register char *t)
{
  register char x;

  for (;;) {
    x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
    x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
    x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
    x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
  }
  return ((int)(unsigned int)(unsigned char) x)
       - ((int)(unsigned int)(unsigned char) *t);
}
*/
/* file: str_diffn.c */
/*
int str_diffn(register char *s,register char *t,unsigned int len)
{
  register char x;

  for (;;) {
    if (!len--) { return 0; } x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
    if (!len--) { return 0; } x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
    if (!len--) { return 0; } x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
    if (!len--) { return 0; } x = *s; if (x != *t) { break; } if (!x) { break; } ++s; ++t;
  }
  return ((int)(unsigned int)(unsigned char) x)
       - ((int)(unsigned int)(unsigned char) *t);
}
*/
// alternative to replace functions 'str_diff' and 'str_diffn':
int str_diff(char *s,char *t) { return byte_diff(s,str_len(s),t); }

/* file: str_len.c */
/*
unsigned int str_len(register char *s)
{
  register char *t;

  t = s;
  for (;;) {
    if (!*t) { return t - s; } ++t;
    if (!*t) { return t - s; } ++t;
    if (!*t) { return t - s; } ++t;
    if (!*t) { return t - s; } ++t;
  }
}
*/
/* file: str_rchr.c */
/* Alternative:  */
unsigned int str_rchr(const char *s, int c) { return byte_rchr(s,str_len(s),c); }

/*
unsigned int str_rchr(register char *s,int c)
{
  register char ch;
  register char *t;
  register char *u;

  ch = c;
  t = s;
  u = 0;
  for (;;) {
    if (!*t) { break; } if (*t == ch) { u = t; } ++t;
    if (!*t) { break; } if (*t == ch) { u = t; } ++t;
    if (!*t) { break; } if (*t == ch) { u = t; } ++t;
    if (!*t) { break; } if (*t == ch) { u = t; } ++t;
  }
  if (!u) u = t;
  return u - s;
}
*/


/* file: str_start.c */
int str_pcmp(register char *s,register char *t) {
  register char x;

  for (;;) {
    x = *t++; if (!x) return 1; if (x != *s++) return 0;
    x = *t++; if (!x) return 1; if (x != *s++) return 0;
    x = *t++; if (!x) return 1; if (x != *s++) return 0;
    x = *t++; if (!x) return 1; if (x != *s++) return 0;
  }
}
