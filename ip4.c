/*
 *  Revision 20171211, Kai Peter
 *  - gcc-6 compatibility: prevent '-Wmisleading-indentation' warning
 *  
 *  - added function 'ip4_bitstring'
 *  Revision 20160729, Kai Peter
 *  - new file: consolidated 'ip4_fmt.c', 'ip4_scan.c' from ucspi-tcp-0.88
 *  - added function 'ip_scanbracket' from original (qmail) 'ip.c'
*/
//#include "fmt.h"
//#include "scan.h"
#include "str.h"
#include "ip.h"

/**
 @brief ip4_fmt
        converts IPv4 address to dotted decimal string format
 @param input:  pointer to IPv4 ip_address struct
        output: pointer to IPv4 address string
 @return int length of address (ok > 0)
 */

/* file: ip4_fmt.c ***************************************************** */
unsigned int ip4_fmt(char *s,char ip[4])
{
  unsigned int len;
  unsigned int i;

  len = 0;
  i = fmt_ulong(s,(unsigned long) (unsigned char) ip[0]); len += i; if (s) s += i;
  if (s) { *s++ = '.'; } ++len;
  i = fmt_ulong(s,(unsigned long) (unsigned char) ip[1]); len += i; if (s) s += i;
  if (s) { *s++ = '.'; } ++len;
  i = fmt_ulong(s,(unsigned long) (unsigned char) ip[2]); len += i; if (s) s += i;
  if (s) { *s++ = '.'; } ++len;
  i = fmt_ulong(s,(unsigned long) (unsigned char) ip[3]); len += i; if (s) s += i;
  return len;
}

/**
 @brief ip4_scan
        parse IPv4 address string and convert to IP address array
 @param input:  pointer to IPv4 address string
        output: pointer to IPv4 ip_address struct
 @return int lenth of ip_address (ok > 0)
 */

/* file: ip4_scan.c **************************************************** */
unsigned int ip4_scan(char *s,char ip[4])
{
  unsigned int i;
  unsigned int len;
  unsigned long u;

  len = 0;
  i = scan_ulong(s,&u); if (!i) { return 0; } ip[0] = u; s += i; len += i;
  if (*s != '.') { return 0; } ++s; ++len;
  i = scan_ulong(s,&u); if (!i) { return 0; } ip[1] = u; s += i; len += i;
  if (*s != '.') { return 0; } ++s; ++len;
  i = scan_ulong(s,&u); if (!i) { return 0; } ip[2] = u; s += i; len += i;
  if (*s != '.') { return 0; } ++s; ++len;
  i = scan_ulong(s,&u); if (!i) { return 0; } ip[3] = u; s += i; len += i;
  return len;
}

/* Taken from the original 'ip.c', used by 'qmail-smtpd' and the original
 * 'dns.c' (from qmail) --> should be replaced (like in 'ucspi-tcp')   */
/*
  Params: *s is an IP address enclosed in brackets
*/
//unsigned int ip_scanbracket(char *s,char ip[4])
/*
unsigned int ip4_scanbracket(char *s,char ip[4])
{
  unsigned int len;

  if (*s != '[') return 0;
  len = ip4_scan(s + 1,ip);
  if (!len) return 0;
  if (s[len + 1] != ']') return 0;
  return len + 2;
}
*/

/**
 @brief ip_scanbracket
        parse IPv4 or IPv6 string address enclosed in brackets
 @param input:  pointer to IPv4/IPv6 address string
        output: IPv4/IPv6 address char array
 @return int length of ip_address (ok > 0)
 */

unsigned int ip_scanbracket(char *s,char *ip_str)
{
  unsigned int len;

  if (*s != '[') return 0;
  if (str_chr(ip_str,':')) {
     len = ip6_scan(s + 1,ip_str);
  } else {
     len = ip4_scan(s + 1,ip_str);
  }

  if (!len) return 0;
  if (s[len + 1] != ']') return 0;
  return len + 2;
}

/**
 @brief ip4_bitstring
        parse IPv4 address and represent as char string with length prefix
 @param input:  IPv4 address char array, prefix length
        output: pointer to stralloc bitstring
 @return 0, if ok; 1, memory shortage; 2, input error
 */

unsigned int ip4_bitstring(stralloc *ip4string,char ip[4],int prefix)
{
  int i, j;
  unsigned char number;

  ip4string->len = 0;
  if (!stralloc_copys(ip4string,"")) return 1;
  if (!stralloc_readyplus(ip4string,32)) return 1;

  for (i = 0; i < 4; i++) {
    number = (unsigned char) ip[i];
    if (number > 255) return 2;

    for (j = 7; j >= 0; j--) {
      if (number & (1<<j)) {
        if (!stralloc_cats(ip4string,"1")) return 1;
      } else {
        if (!stralloc_cats(ip4string,"0")) return 1;
      }
      if (prefix == 0) {
        if (!stralloc_0(ip4string)) return 1;
        else return 0;
      }
      prefix--;
    }
  }
  if (!stralloc_0(ip4string)) return 1;

  return 0;
}
