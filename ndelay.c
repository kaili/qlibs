#include <sys/types.h>
#include <fcntl.h>
#include "ndelay.h"

#ifndef O_NONBLOCK
#define O_NONBLOCK O_NDELAY
#endif

int ndelay(int fd, unsigned int mode) {
  if (mode == ON) {
    /* put fd in non-blocking mode (old: ndelay_on) */
    return fcntl(fd,F_SETFL,fcntl(fd,F_GETFL,0) | O_NONBLOCK);
  } else if (mode == OFF) {
    /* (else) put fd in blocking mode (old: ndelay_off) */
    return fcntl(fd,F_SETFL,fcntl(fd,F_GETFL,0) & ~O_NONBLOCK);
  }
  return -1;
}

/*
int ndelay_on(int fd) {
  return fcntl(fd,F_SETFL,fcntl(fd,F_GETFL,0) | O_NONBLOCK);
}

int ndelay_off(int fd) {
  return fcntl(fd,F_SETFL,fcntl(fd,F_GETFL,0) & ~O_NONBLOCK);
}
*/
