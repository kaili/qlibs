/*
 *  Revision 20180320, Kai Peter
 *  - conslidated socket_bind.c
 *  Revision 20180306, Kai Peter
 *  - conslidated socket_accept.c, socket_info.c
 *  - moved all 'setsockopt' functions to socket_options.c
 *  Revision 20160728, Kai Peter
 *  - new file: consolidated some smaller files:
 *    socket_getifname.c, socket_opts.c, socket_v6any.c, socket_delay.c,
 *    socket_ip4loopback.c, socket_v6loopback.c, socket_getifidx.c,
 *    socket_listen.c, socket_v4mappedprefix.c
*/
#include <net/if.h>
#include "qsocket.h"
#include "byte.h"
#include "str.h"
#include "errmsg.h"

const unsigned char V4loopback[4] = {127,0,0,1};
/* the 'V4mappedprefix' constant is needed by 'ip.a' too */
const unsigned char V4mappedprefix[12]={0,0,0,0,0,0,0,0,0,0,0xff,0xff};
const unsigned char V6localnet[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const unsigned char V6loopback[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};

int ipv4socket;

/* socket_listen: *********************************************************** */
int socket_listen(int s, int backlog) { return listen(s,backlog); }

/* get name or index of network interface *********************************** */
static char ifname[IFNAMSIZ];

const char* socket_getifname(uint32 interface) {
  char *tmp = if_indextoname(interface,ifname);
  if (tmp)
    return tmp;
  else
    return "[unknown]";
}

uint32 socket_getifidx(const char* ifname) { return if_nametoindex(ifname); }

/* socket_accept: *********************************************************** */
int socket_accept(int s, char ip[16], uint16 *port, uint32 *scope_id) {
  struct sockaddr_in6 sa;
  unsigned int dummy = sizeof(sa);
  int fd;

  fd = accept(s,(struct sockaddr *)&sa,&dummy);
  if (fd == -1) return -1;

  if (sa.sin6_family == AF_INET) {
    struct sockaddr_in *sa4=(struct sockaddr_in*)&sa;
    byte_copy(ip,12,V4mappedprefix);
    byte_copy(ip+12,4,(char *)&sa4->sin_addr);
    uint16_unpack_big((char *)&sa4->sin_port,port);
    return fd;
  }
  byte_copy(ip,16,(char *)&sa.sin6_addr);
  uint16_unpack_big((char *)&sa.sin6_port,port);
  if (scope_id)*scope_id=sa.sin6_scope_id;

  return fd;
}

/* socket_info: ************************************************************* */
/* use wrapper functions 'socket_local' and 'socket_remote' to call qsockinfo */
int qsockinfo(int s, char ip[16], uint16 *port, uint32 *scope_id, size_t info) {
  struct sockaddr_in6 sa;
  unsigned int dummy = sizeof(sa);
  if (info) {   /* remote */
  	if (getpeername(s,(struct sockaddr *)&sa,&dummy) == -1) return -1;
  } else {      /* local */
    if (getsockname(s,(struct sockaddr *)&sa,&dummy) == -1) return -1;
  }

//  if (sa.sin6_family == AF_INET) {
  if (ipv4socket) {
    struct sockaddr_in *sa4=(struct sockaddr_in*)&sa;
    byte_copy(ip,12,V4mappedprefix);
    byte_copy(ip+12,4,(char *)&sa4->sin_addr);
    uint16_unpack_big((char *)&sa4->sin_port,port);
    if (scope_id) *scope_id = 0;
    return 0;
  }
  byte_copy(ip,16,(char *)&sa.sin6_addr);
  uint16_unpack_big((char *)&sa.sin6_port,port);
  if (scope_id) *scope_id = sa.sin6_scope_id;

  return 0;
}

/* socket option: set TCP_NODELAY  ****************************************** */
int socket_tcpnodelay(int s) {
  int opt = 1;
  return setsockopt(s,IPPROTO_TCP,1,&opt,sizeof opt); /* 1 == TCP_NODELAY */
}

/* socket_bind: ************************************************************* */
int socket_bind(int s, const char ip[16], uint16 port, uint32 scope_id) {
  struct sockaddr_in6 sa;

  if (ipv4socket) {    //  { return socket_bind4(s,ip+12,port); }
    struct sockaddr_in sa;

	byte_zero(&sa,sizeof(sa));
	sa.sin_family = AF_INET;
	uint16_pack_big((char *)&sa.sin_port,port);
	byte_copy((char *)&sa.sin_addr,4,ip);

	return bind(s,(struct sockaddr *)&sa,sizeof(sa));
  }

  byte_zero(&sa,sizeof(sa));
  sa.sin6_family = AF_INET6;
  uint16_pack_big((char *) &sa.sin6_port,port);
/*  implicit: sa.sin6_flowinfo = 0; */
  byte_copy((char *)&sa.sin6_addr,16,ip);
  sa.sin6_scope_id = scope_id;

  return bind(s,(struct sockaddr *) &sa,sizeof(sa));
}

/* socket option: reuse ip address and - if available - port */
int socket_reuse(int s) {
  int opt = 1;
  if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) return -1;
//  return setsockopt(s,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));
#ifdef SO_REUSEPORT     /* available since linux 3.9 */
  if (setsockopt(s, SOL_SOCKET, SO_REUSEPORT, &opt, sizeof(opt)) < 0) return -1;
#endif
  return 0;
}
