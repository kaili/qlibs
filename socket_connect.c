/*
 *  Revision 20180723, Kai Peter
 *  - added 'last' ipv4socket check to function 'socket_connect()'
 *  Revision 20180411, Kai Peter
 *  - removed 'socket_connect4' function
 *  - changed 'ndelay' function
 *  Revision 20180305, Kai Peter
 *  - removed 'LIBC_HAS_IP6'
 *  - added function timeoutconn
 *  Revision 20170213, Kai Peter
 *  - function socket_connect4(): changed 'char' to 'unsigned char'
 *  Revision 20160728, Kai Peter
 *  - new file: consolidated 'socket_conn6.c', 'socket_conn.c'
*/
#include "socket.h"
#include "ip.h"
//#include "byte.h"
#include "errmsg.h"
#include "ndelay.h"
#include "iopause.h"

int socket_connect(int s, const char ip[16], uint16 port, uint32 scope_id) {
  struct sockaddr_in6 sa;

  if (!ipv4socket) ipv4socket = ip6_isv4mapped(ip);   // last check

  if (ipv4socket) {
    struct sockaddr_in sa;

	byte_zero(&sa,sizeof sa);
	sa.sin_family = AF_INET;
	uint16_pack_big((char *) &sa.sin_port,port);
	byte_copy((char *) &sa.sin_addr,4,ip);

	return connect(s,(struct sockaddr *) &sa,sizeof sa);
  }

  byte_zero(&sa,sizeof sa);
  sa.sin6_family = PF_INET6;
  uint16_pack_big((char *) &sa.sin6_port,port);
  sa.sin6_flowinfo = 0;
  sa.sin6_scope_id = scope_id;
  byte_copy((char *) &sa.sin6_addr,16,ip);

  return connect(s,(struct sockaddr *) &sa,sizeof sa);
}

/* this explizit declaration is needed to prevent compiler warnings */
int read(int _str, void *_buf, int _b);

int socket_connected(int s)
{
  struct sockaddr_in sa;
  int dummy;
  char ch;

  dummy = sizeof sa;
  if (getpeername(s,(struct sockaddr *) &sa,(socklen_t *)&dummy) == -1) {
    read(s,&ch,1); /* sets errno */
    return 0;
  }
  return 1;
}

int timeoutconn(int s, char ip[16], uint16 port, unsigned int timeout, uint32 netif) {
  struct taia now;
  struct taia deadline;
  iopause_fd x;

  if (socket_connect(s,ip,port,netif) == -1) {
    if ((errno != EWOULDBLOCK) && (errno != EINPROGRESS)) return -1;
    x.fd = s;
    x.events = IOPAUSE_WRITE;
    taia_now(&now);
    taia_uint(&deadline,timeout);
    taia_add(&deadline,&now,&deadline);
    for (;;) {
      taia_now(&now);
      iopause(&x,1,&deadline,&now);
      if (x.revents) break;
      if (taia_less(&deadline,&now)) {
    errno = ETIMEDOUT; /* note that connect attempt is continuing */
    return -1;
      }
    }
    if (!socket_connected(s)) return -1;
  }

  if (ndelay(s,OFF) == -1) return -1;
  return 0;
}
