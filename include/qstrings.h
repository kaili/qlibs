/* high level string functions on top of str.c and fmt.c */
/*
 *
 *
*/
#ifndef QSTRINGS_H
#define QSTRINGS_H

#include "fmt.h"
#include "strlen.h"
#include "str.h"
#include "stralloc.h"
#include "errmsg.h"

char *fmtnum(long n);
#define qstrnum fmtnum

#ifndef QLIBS_LEFT_RIGHT
#define QLIBS_LEFT_RIGHT
#define QLEFT  0   /* left  */
#define QRIGHT 1   /* right */
#endif
char *strsplit(char *s, unsigned int pos, int part);

size_t stralloc_split(stralloc *sa, unsigned int pos, int part);
#define sasplit stralloc_split

ssize_t strcats(char **dst, char *src);
#define strcpys(dst,src) ((*dst = ""), (strcats(dst,src)))
/* compare the whole string, not "prefixes" only */
#define strequal(s,t) (!str_diff((s),(t))) && (!str_diff((t),(s)))
//#define str_equal strequal

int chrpl(char **s, int c, int x);
char *chrpls(char *s, int c, int x);
#define chrmv(s,c,x) ((x = -1), chrpl(s,c,x))
#define chrmvs(s,c) chrpls(s,c,-1)


char *str_rtrim(char *s, char c);
char *str_ltrim(char *s, char c);
#define rtrim(s) str_rtrim(s,' ')     /* remove trailing spaces */
#define ltrim(s) str_ltrim(s,' ')     /* remove leading spaces  */
char *trim(char *s);      /* with 'char *' a macro doesn't work */

int instr(char *,char *);

#endif
