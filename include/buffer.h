/*
 *  Revision 20180222, Kai Peter
 *  - renamed 'buffer_put()' --> 'buffer_putb()' to follow usual conventions
 *  Revision 20170918; Kai Peter
 *  - added prototypes buffer_read() and buffer_write()
 *  Revision 20160708, Kai Peter
 *  - changed 'int (*op)();' to 'ssize_t (*op)();'
*/
#ifndef BUFFER_H
#define BUFFER_H
#include <sys/types.h>  /* need type: ssize_t */

typedef struct buffer {
  char *x;              /* actual buffer space */
  unsigned int p;		/* current position */
  unsigned int n;		/* current size of string in buffer */
  int fd;
  ssize_t (*op)();      /* operation: read or write (both returns ssize_t) */
} buffer;

#define BUFFER_INIT(op,fd,buf,len) { (buf), 0, (len), (fd), (op) }
#define BUFFER_INSIZE 8192
#define BUFFER_OUTSIZE 8192

extern void buffer_init(buffer *b,ssize_t (*op)(),int fd,char *s,unsigned int len);

extern int buffer_flush(buffer *);
extern int buffer_putb(buffer *,char *,unsigned int);
#define buffer_put buffer_putb		/* backwards compat (see revision 20180222 */
extern int buffer_putalign(buffer *,char *,unsigned int);
extern int buffer_putflush(buffer *,char *,unsigned int);
extern int buffer_puts(buffer *,char *);
extern int buffer_putsalign(buffer *,char *);
extern int buffer_putsflush(buffer *,char *);

#define buffer_PUTC(s,c) \
  ( ((s)->n != (s)->p) \
    ? ( (s)->x[(s)->p++] = (c), 0 ) \
    : buffer_put((s),&(c),1) \
  )

extern int buffer_get(buffer *,char *,unsigned int);
extern int buffer_feed(buffer *);

extern char *buffer_peek(buffer *);
extern void buffer_seek(buffer *,unsigned int);

#define buffer_PEEK(s) ( (s)->x + (s)->n )
#define buffer_SEEK(s,len) ( ( (s)->p -= (len) ) , ( (s)->n += (len) ) )

#define buffer_GETC(s,c) \
  ( ((s)->p > 0) \
    ? ( *(c) = (s)->x[(s)->n], buffer_SEEK((s),1), 1 ) \
    : buffer_get((s),(c),1) \
  )

extern int buffer_copy(buffer *,buffer *);

//extern ssize_t buffer_read(int,char *,unsigned int);
//extern ssize_t buffer_write(int,char *,unsigned int);
/* some 'older' programs have these (useless) functions */
//#define buffer_unixread read(int,char *,unsigned int)
//#define buffer_unixwrite write(int,char *,unsigned int)

extern buffer *buffer_0;
extern buffer *buffer_1;
extern buffer *buffer_2;

#endif
