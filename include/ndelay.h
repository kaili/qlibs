#ifndef NDELAY_H
#define NDELAY_H

#ifndef QLIBS_ONOFF_SWITCH
#define QLIBS_ONOFF_SWITCH
#define ON  1
#define OFF 0
#endif

extern int ndelay(int fd, unsigned int mode);
#define ndelay_on(fd) ndelay((fd),ON)
#define ndelay_off(fd) ndelay((fd),OFF)

#endif
