#ifndef OPEN_H
#define OPEN_H

extern int open_read();
extern int open_excl();
extern int open_append();
extern int open_trunc();
extern int open_write();

extern int fopen();
#define qopen(f,m) ((m = 'R'), (open_read(f))
//#define popenw(f,W) ((*W = "W"), (open_write(f))

#endif
