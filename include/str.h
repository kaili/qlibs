/*
 * Revision 20190712, Kai Peter
 * - adjusted declarations, renamed 'str_start' --> 'str_pcmp'
 * - commented out deprecated 'str_diff' and str_diffn' functions
*/
#ifndef STR_H
#define STR_H

#include "byte.h"
#include "stralloc.h"

extern unsigned int str_copy();

//extern int str_diff();
extern int str_diff(char *s,char *t);
#define str_diffn(s,t,n) byte_diff((s),(n),(t))
//#define str_equal(s,t) (!str_diff((s),(t)))
#define str_equal strequal
//extern int str_diffn();

//extern unsigned int str_len();
extern unsigned int str_chr();
#define str_chr1(s,t) byte_chr((s),str_len(s),(t))
extern unsigned int str_rchr();
extern int str_pcmp(char *,char *);
#define str_start(s,t) str_pcmp(s,t)    /* backwards compat */

#endif
