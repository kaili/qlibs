/*
 *  Revision 20180313, Kai Peter
 *  - reworked prototype declarations
 *  - added '#include <sys/types.h>' for new functions timeout{read,write}
 *  - added 'extern int ipv4socket' (thanks Erwin Hoffmann)
 *  Revision 20160728, Kai Peter
 *  - switched to 'uint_t.h'
 *  - changed 'socket_tcp' --> 'socket_tcp4', 'socket_udp' --> 'socket_udp4'
*/
#ifndef SOCKET_H
#define SOCKET_H

#include <sys/types.h>  /* need ssize_t */
#include <sys/param.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include "uint_t.h"

extern int socket_tcp4(void);
extern int socket_tcp6(void);
extern int socket_udp4(void);
extern int socket_udp6(void);
#define socket_tcp socket_tcp6
#define socket_udp socket_udp6

extern int qconnect(int s, const char ip[16], uint16 port, uint32 scope_id);
#define socket_connect qconnect
#define socket_connect6 socket_connect
extern int socket_connected(int);

extern int qbind(int s, const char ip[16], uint16 port, uint32 scope_id);
#define socket_bind qbind
#define socket_bind6 socket_bind    // remoteinfo6 (temp. compat)

extern int qlisten(int s, int backlog);
extern int qaccept(int s, char ip[16], uint16 *port, uint32 *scope_id);
#define socket_listen qlisten
#define socket_accept qaccept

//extern int socket_recv4(int,char *,int,char *,uint16 *);
ssize_t socket_recv(int s, char *buf, size_t len, const char ip[16], uint16 *port, uint32 *scope_id);
//extern int socket_send4(int,const char *,int,const char *,uint16);
ssize_t socket_send(int s, const char *buf, size_t len, const char ip[16], uint16 port, uint32 scope_id);

extern int qsockinfo(int s, char ip[16], uint16 *port, uint32 *scope_id, size_t info);
#define socket_local(s,ip,port,scope_id)  qsockinfo((s),(ip),(port),(scope_id),0)
#define socket_remote(s,ip,port,scope_id) qsockinfo((s),(ip),(port),(scope_id),1)

/* the following is not used yet: */
/* enable sending udp packets to the broadcast address */
//extern int socket_broadcast(int);
/* join a multicast group on the given interface */
//extern int socket_mcjoin4(int,char *,char *);
//extern int socket_mcjoin6(int,char *,int);
/* leave a multicast group on the given interface */
//extern int socket_mcleave4(int,char *);
//extern int socket_mcleave6(int,char *);
/* set multicast TTL/hop count for outgoing packets */
//extern int socket_mcttl4(int,char);
//extern int socket_mcttl6(int,char);
/* enable multicast loopback */
//extern int socket_mcloop4(int,char);
//extern int socket_mcloop6(int,char);

extern const char* socket_getifname(uint32 interface);
extern uint32 socket_getifidx(const char *ifname);

extern int ipv4socket;

extern int socket_reuse(int s);
// devel: test with tcpserver
#define socket_bind_reuse(s,ip,p,scope_id) (socket_reuse((s)), qbind((s),(ip),(p),(scope_id)))
extern int socket_tcpnodelay(int);

extern int timeoutconn(int fd, char ip[16], uint16 port, unsigned int timeout, uint32 scope_id);
extern int timeoutread(int fd, char *buf, size_t len, unsigned int timeout);
extern int timeoutwrite(int fd, char *buf, size_t len, unsigned int timeout);

#endif
