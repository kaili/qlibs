/*
 *  Revision 20200716, Kai Peter
 *  - removed NOLOGSTR
 *  - added default definition of ME/WHO
 *  - added switch DJBCOMPAT - right now it is defined always
 *  - added macro 'out' - ok, this seems to be a bit misused here but errmsg is
 *    usually included/linked always
 *  - 'ldbg' have to be activated through QDEBUG now
 *  Revision 20180322, Kai Peter
 *  - changed macro definitions (consolidation)
 *  - change DEF_LOGLEVEL to NOTICE
*/
#ifndef ERROR_H
#define ERROR_H

#include <errno.h>
#include <sys/types.h>

#ifndef EPROTO             /* OpenBSD compat */
#define EPROTO EINTR
#endif

#define DJBCOMPAT 1
#ifdef DJBCOMPAT      /* djb backwards compatibility for error lib - deprecated */
                                    /* Comparison of error codes and constants:
                                             intern   Linux  FreeBSD            */
#define error_intr          EINTR         /*    -1       4       4     */
#define error_nomem         ENOMEM        /*    -2      12      12     */
#define error_noent         ENOENT        /*    -3       2       2     */
#define error_txtbsy        ETXTBSY       /*    -4      26      26     */
#define error_io            EIO           /*    -5       5       5     */
#define error_exist         EEXIST        /*    -6      17      17     */
#define error_timeout       ETIMEDOUT     /*    -7     110      60     */
#define error_inprogress    EINPROGRESS   /*    -8     115      36     */
#define error_wouldblock    EWOULDBLOCK   /*    -9    EAGAIN  EAGAIN   */
#define error_again         EAGAIN        /*   -10      11      35     */
#define error_pipe          EPIPE         /*   -11      32      32     */
#define error_perm          EPERM         /*   -12       1       1     */
#define error_acces         EACCES        /*   -13      13      13     */
//extern int error_nodevice;  ENXIO       /*   -14      (6)     (6)    */
#define error_proto         EPROTO        /*   -15      71      92     */
#define error_isdir         EISDIR        /*   -16      21      21     */
#define error_connrefused   ECONNREFUSED  /*   -17     111      61     */
//extern int error_notdir;    ENOTDIR     /*   -18      20      20     */
#define error_rofs          EROFS         /*   -19      30      30     */
#define error_connreset     ECONNRESET    /*   -20     104      54     */

#define error_str(i) errstr(i)
extern int error_temp();   /* deprecated */
#endif    /* end djb backwards compatibility error lib */


#ifndef ME
#define ME ""           /* set an empty value as default or use WHO if defined */
  #ifdef WHO
  #define ME WHO
  #endif
#endif

/* severity constants (similar to RfC 5424) */
#define FATAL     0     /* ERMERGENCY (FATAL) */
#define ALERT     1     /* ALERT */
#define CRIT      2     /* CRITICAL */
#define ERROR     3     /* ERROR */
#define WARN      4     /* WARNING */
#define NOTICE    5     /* NOTICE */
#define INFO      6     /* INFO */
#define DEBUG     7     /* DEBUG */

#define DEF_LOGLEVEL NOTICE
extern unsigned int loglevel;

extern size_t errmsg(char *who, int errorcode, unsigned int severity, char *emsg);
extern char *errstr(int code);

/* build error message from multiple partial strings */
extern char *build_error_msg(char *[]);
#define B(...) build_error_msg((char *[]){__VA_ARGS__,NULL})

/* useful combinations of params (abbreviations) */
/* severity: FATAL (hard) - system error */
#define errsys(c) errmsg(ME,c,FATAL,"")
/* severity: ERROR (temp) - soft/user error */
#define errtmp(c) errmsg(ME,c,ERROR,"")
/* 'int'ernal definition of code and an individual error message */
#define errint(c,s) errmsg(ME,c,ERROR,s)  /* standard usage */
/* severity: facultative */
#define errlog(c,l,s) errmsg(ME,c,l,s)      /* be more flexible */

/* severity: NOTICE - log messages (we cast the function type to void here) */
#define log(s) (void)errmsg(ME,0,NOTICE,s)              /* standard logging */
#define out(s) (void)errmsg("",0,0,s)           /* print 's' to stdout only */

#ifdef QDEBUG                 /* enable debug messages if QDEBUG is defined */
#define ldbg(s) ((loglevel = DEBUG), (void)errmsg("",0,DEBUG,s))
#endif

/* backwards compat: djb uses internal codes and some programs rely on it   */
#define ESOFT  -100     /* soft error, reversed negative */
#define EHARD  -111     /* hard error, reversed negative */
#define errsoft(s) errmsg(ME,ESOFT,CRIT,s)  // re-think severity here!
#define errhard(s) errmsg(ME,EHARD,ALERT,s)

#define errmem if (errno) errsys(ENOMEM);     /* mostly used error handling */
#define logmem errlog(ENOMEM,WARN,"")

#endif
