/*
 *  Revision 20160628, Kai Peter
 *  - updated code (*env_get) like in ucspi-tcp-0.88
 *  - commented out 'env_pick' and 'env_clear'
*/
#ifndef ENV_H
#define ENV_H

extern char **environ;

extern int env_isinit;
extern int env_init();

extern /*@null@*/char *env_get(char *);
extern int env_put(char *s,char *t);
extern int env_puts(char *s);
extern int env_unset(char *s);
extern char *env_pick();
extern void env_clear();

extern char *env_findeq(char *s);

#define env_put2 env_put /* backwards compatibility */

#endif
