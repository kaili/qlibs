/*
 *
 *  Revision 20210129, Kai Peter
 *  - added some comments
 *  - new function: chrpl()
 *  - extended function strsplit: added check of 'L'
 *  Revision 20200607, Kai Peter
 *  - added several new functions
 *  Revision 20180410, Kai Peter
 *  - new file
*/
#include <unistd.h>
#include "qstrings.h"
#include "errmsg.h"
#include "str.h"

/* fmtnum is a replacement/abbreviation of the code snippet 'strnum'. It is no
 * longer required to define 'char strnum[]' in code and it is shorter to use.
 * Example:
 *       log(B(num is: "fmtnum(num)));                                      */
char *fmtnum(register long n) {
  char num[FMT_SLONG];
  stralloc sa = {0};

  num[fmt_slong(num,n)] = 0;
  if (!stralloc_copys(&sa,num)) errmem;
  if (!stralloc_0(&sa)) errmem;
  return (sa.s);
}
/* Split a string in its left or right part at position 'pos'. The char at pos
   will be lost. To keep this char use 'pos+/-1' in the caller function.
 */
char *strsplit(char *s, unsigned int pos, int part) {
  stralloc sa = {0};
  int i;

  if ((!part) || (part == 'L')) {
    if (stralloc_copyb(&sa,s,pos) < 1) { errmem; } /* get first (left) part */
  } else {
    for(i = 0; i <= str_len(s); ++i) {             /* get last (right) part */
      if ( i > pos ) if (stralloc_catb(&sa,&s[i],1) < 1) { errmem; };
    }
  }
  if (!stralloc_0(&sa)) { errmem; }
  return (sa.s);
}

/* strcats: similar to the 'strcat' C standard function ******************* */
ssize_t strcats(char **dst, char *src) {
  static stralloc sa = {0};
  ssize_t len;

  char *s = *dst;
  if (!stralloc_copys(&sa,s)) return -1;
  if (!stralloc_cats(&sa,src)) return -1;
  len = sa.len;    /* length w/o \0 */
  if (!stralloc_0(&sa)) return -1;
  *dst = sa.s;
  return (len);
}

/* devel: similar to strsplit but with stralloc (experimental) */
size_t stralloc_split(stralloc *sa, unsigned int pos, int part) {
  char *x = sa->s;
  int i;

  if (!part) {
    if (stralloc_copyb(sa,x,pos) < 1) { errmem; }  /* get first (left) part */
  } else {
      stralloc_init(sa);
      for(i = 0; i <= str_len(x); ++i) {           /* get last (right) part */
        if ( i > pos ) if (stralloc_catb(sa,&x[i],1) < 1) { errmem; };
      }
  }
  if (!stralloc_0(sa)) { errmem; }
  return (0);
}

/* replace all chars c by char x from string *s, return s (testing) */
char *chrpls(char *s, int c, int x) {
  char ch;
  int i;
  stralloc sa = {0};

  for (i=0;i < str_len(s);++i) {
    ch = s[i];
    switch (x) {
    case -1: if (ch != c) if (!stralloc_catb(&sa,&ch,1)) errmem; break;
    default: if (ch == c) ch = x; if (!stralloc_catb(&sa,&ch,1)) errmem; break;
    }
  }
  if (!stralloc_0(&sa)) errmem;
  return(sa.s);
}

int chrpl(char **s, int c, int x) {
  char ch;
  int i;
  int len;
  stralloc sa = {0};

  char *t = *s;
  len = str_len(t);
  for (i=0;i < len;++i) {
    ch = t[i];
    switch (x) {
    case -1: if (ch != c) if (!stralloc_catb(&sa,&ch,1)) errmem; break;
    default: if (ch == c) ch = x; if (!stralloc_catb(&sa,&ch,1)) errmem; break;
    }
  }
  if (!stralloc_0(&sa)) errmem;
  *s = sa.s;
  return(len - str_len(sa.s));
}


/* remove all trailing chars 'x' from string 's' */
char *str_rtrim(char *s, char x) {
  char ch;
  int i;
  stralloc sa = {0};

  for (i = str_len(s);i>0;i--) {
    ch = s[i-1]; if (ch != x) break;
  }
  if (!stralloc_copys(&sa,strsplit(s,i,0))) errmem; /* split: get left part */
  if (!stralloc_0(&sa)) errmem;
  return(sa.s);
}
/* remove all leading chars 'x' from string 's' */
char *str_ltrim(char *s, char x) {
  char ch;
  int i;
  stralloc sa = {0};

  for (i=0;i < str_len(s);++i) {
    ch = s[i]; if (ch != x) break;
  }
  while(i < str_len(s)) {
    ch = s[i];
    if (!stralloc_catb(&sa,&ch,1)) errmem;
    i++;
  }
  if (!stralloc_0(&sa)) errmem;
  return(sa.s);
}
/* remove all surrounding spaces */
char *trim(char *s) {
  s = ltrim(s); s = rtrim(s);
  return(s);
}

/* The instr() functions checks if string *t is in string *s.  String *t could
   be longer than a single char (against the byte_chr() function).          */
int instr(char *s,char *t) {
  char ch;
  unsigned int len;
  int i;

  len = str_len(s);
  while (str_len(s) >= str_len(t)) {
    i = len - str_len(s) + 1;   /* with offset +1 */
    if (str_pcmp(s,t) == 1) { return(i); }
    ch = s[0]; s = str_ltrim(s,ch);
  }
  return(0);   /* return false here */
}
